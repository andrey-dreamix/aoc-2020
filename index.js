const utils = require('./utils');

const args = process.argv.slice(2);

if (!args || !args.length) {
    throw new Error('Pleaes provide a task index like: node index.js 1');
}

const taskIndex = args[0];
const task = require(`./tasks/task${taskIndex}.js`);

if (!task.prepare) {
    throw new Error('Each task must export a `prepare` function');
}

const rawInput = utils.input.read(`./inputs/input${taskIndex}.txt`);
const input = task.prepare(rawInput);

if (task.part1) {
    const start = +new Date();
    console.log(`Part 1 answer: ${task.part1(input, rawInput)} (in ${+new Date() - start}ms)`);
}
if (task.part2) {
    const start = +new Date();
    console.log(`Part 2 answer: ${task.part2(input, rawInput)} (in ${+new Date() - start}ms)`);
}