const utils = require('../utils');

function prepare(input) {
    return utils.input.splitLines(input).map((row) => row.split(''));
}

function part1(input) {
    const rowSize = input[0].length;
    let treesEncountered = 0;

    for (let i = 1, j = 3; i < input.length; i++, j += 3) {
        if (input[i][j % rowSize] === '#') {
            treesEncountered++;
        }
    }

    return treesEncountered;
}

function part2(input) {
    const rowSize = input[0].length;

    function getSlopeTrees(right, down) {
        let treesEncountered = 0;

        for (let i = down, j = right; i < input.length; i += down, j += right) {
            if (input[i][j % rowSize] === '#') {
                treesEncountered++;
            }
        }

        return treesEncountered
    }

    return [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]
        .map(([right, down]) => getSlopeTrees(right, down))
        .reduce((result, current) => result * current, 1);
}
    

module.exports = {
    prepare,
    part1,
    part2,
}
