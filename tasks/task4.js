const utils = require('../utils');
const os = require('os');

const requiredFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl','ecl', 'pid'];

function prepare(input) {
  return input
    .split('\n\n')
    .map((row) => {
      const pairs = row.replace(/(?:\r\n|\r|\n)/g, ' ').split(' ');
      const result = {};
      
      pairs.forEach((pair) => {
        const [key, value] = pair.split(':');
        result[key] = value;
      });
      
      return result;
    });
}

function part1(input, rawInput) {
  return rawInput
    .split('\n\n') // splits by passport
    .filter((passport) => 
      passport.replace(/\n/g, ' ') // removes newlines -> becomes a row
      .replace(/cid:\d+ ?|[^:]/g, '') // removes cid:XXX (with or without trailing space); removes everything except colons
      .length === 7 // filter true only if string length is 7 characters
    ).length;
  
  // or
  return input
    .filter((item) => 
      Object.keys(item)
      .filter((key) => key !== 'cid')
      .length === 7
    )
    .length;
}

function part2(input) {
  const eyeColors = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'];
  return input
    .filter((item) => Object.keys(item).filter((key) => key !== 'cid').length === 7)
    .filter((item) => {
      delete item.cid;

      item.byr = Number(item.byr.replace(/\D/g, ''));
      if (item.byr < 1920 || item.byr > 2002) {
        return false;
      }
      
      item.iyr = Number(item.iyr.replace(/\D/g, ''));
      if (item.iyr < 2010 || item.iyr > 2020) {
        return false;
      }
      
      item.eyr = Number(item.eyr.replace(/\D/g, ''));
      if (item.eyr < 2020 || item.eyr > 2030) {
        return false;
      }

      const height = Number(item.hgt.replace(/\D/g, ''));
      if (item.hgt.indexOf('cm') > 0) {
        if (height < 150 || height > 193) {
          return false;
        }
      } else if (item.hgt.indexOf('in') > 0) {
        if (height < 59 || height > 76) {
          return false;
        }
      } else {
        return false;
      }
      
      if (item.hcl.replace(/[^#a-f0-9]/g, '').length !== 7) {
        return false;
      }

      if (!eyeColors.includes(item.ecl)) {
        return false;
      }

      if (item.pid.replace(/[^0-9]/g, '').length !== 9) {
        return false;
      }

      return true;
    }).length;
}


module.exports = {
    prepare,
    part1,
    part2,
}
