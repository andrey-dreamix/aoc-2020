const utils = require('../utils');

function prepare(input) {
    return utils.input.splitLines(input);
}

function part1(input) {
    function passwordIsValid(line) {
        const parts = line.split(' ');
        const [min, max] = parts[0].split('-');
        const char = parts[1][0];
        const pwd = parts[2];

        const stripped = pwd.replace(new RegExp(`[^${char}]`, 'g'), '');
        const length = stripped.length;

        if (length < min) {
            return false;
        } else if (length > max) {
            return false;
        }
        
        return true;
    }

    return input.map(passwordIsValid).filter(Boolean).length;
}

function part2(input) {
    function passwordIsValid(line) {
        const parts = line.split(' ');
        const [firstIndex, secondIndex] = parts[0].split('-');
        const char = parts[1][0];
        const pwd = parts[2];

        const firstValue = pwd[firstIndex - 1];
        const secondValue = pwd[secondIndex - 1];

        if (firstValue === char || secondValue === char) { // any of the two match
            if (firstValue === secondValue) { // both are equal
                return false;
            }
            // one of the two are matching, and the other has a different value
            return true;
        }
        
        return false; // none of the two match
    }

    return input.map(passwordIsValid).filter(Boolean).length;
}

module.exports = {
    prepare,
    part1,
    part2,
}
