const utils = require('../utils');

function prepare(input) {
    return utils.input.splitLines(input).map(Number.parseFloat);
}

function part1(input) {
    const map = {};
    for (let value of input) {
        const diff = 2020 - value;
        if (map[diff]) {
            return value * diff;
        } else {
            map[value] = diff;
        }
    }
}

function part2(input) {}

module.exports = {
    prepare,
    part1,
}
