# Advent of Code 2020

In order to run a specific task, use:

```
node index.js 1
```

Where  `1` is the index of the task. A single task run available at the moment.