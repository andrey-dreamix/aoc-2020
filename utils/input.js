const fs = require('fs');
const os = require('os');

module.exports = {
    read(path) {
        return fs.readFileSync(path, 'utf-8');
    },
    splitLines(raw) {
        return raw.split(os.EOL);
    }
}